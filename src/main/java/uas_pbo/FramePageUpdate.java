package uas_pbo;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import javax.swing.*;

import uas_pbo.models.connection;

public class FramePageUpdate extends MainFrame {
    JTextField FieldSetKolom, FieldKey, FieldWhere, FieldCondition;

    public void Update() {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                JFrame frame = new JFrame("Update");
                frame.setSize(600, 700);
                frame.setMinimumSize(new Dimension(600, 500));
                frame.setVisible(true);
                frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                JPanel panel = new JPanel(new GridLayout(5, 2, 5, 5));
                panel.setOpaque(true);

                JLabel labelSetkolom = new JLabel("Set Kolom");
                labelSetkolom.setFont(mainFont);
                FieldSetKolom = new JTextField();
                FieldSetKolom.setFont(mainFont);

                JLabel LabelSetKey = new JLabel("Insert Isian");
                LabelSetKey.setFont(mainFont);
                FieldKey = new JTextField();
                FieldKey.setFont(mainFont);

                JLabel labelWhere = new JLabel("Posisi");
                labelWhere.setFont(mainFont);
                FieldWhere = new JTextField();
                FieldWhere.setFont(mainFont);

                JLabel labelCondition = new JLabel("Nama Kolom");
                labelCondition.setFont(mainFont);
                FieldCondition = new JTextField();
                FieldCondition.setFont(mainFont);

                JLabel labelData = new JLabel();
                labelData.setLayout(new GridLayout(1, 2, 5, 5));
                labelData.setFont(mainFont);

                JButton btnUpdate = new JButton("Update");
                btnUpdate.setLayout(new GridLayout(1, 2, 5, 5));
                btnUpdate.setFont(mainFont);
                btnUpdate.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String setKolom = FieldSetKolom.getText();
                        String setKey = FieldKey.getText();
                        String where = FieldWhere.getText();
                        String whereKey = FieldCondition.getText();
                        labelData.setText("Kolom : " + setKolom + ", Key : " + setKey + ", Where : " + where
                                + ", Condition : " + whereKey);

                        update(setKolom, setKey, where, whereKey);
                    }
                });

                panel.add(labelSetkolom);
                panel.add(FieldSetKolom);
                panel.add(LabelSetKey);
                panel.add(FieldKey);
                panel.add(labelWhere);
                panel.add(FieldWhere);
                panel.add(labelCondition);
                panel.add(FieldCondition);
                panel.add(labelData);
                panel.add(btnUpdate);

                frame.getContentPane().add(panel);
                frame.pack();
                frame.setLocationByPlatform(true);
                frame.setVisible(true);
                frame.setResizable(false);
            }
        });
    }

    public void update(String setKolom, String key, String where, String whereKey) {
        try {
            connection connection = new connection();
            connection.KoneksiMySQL();
            conn = connection.getCon();

            String insertQuery;
            PreparedStatement ps;

            if (setKolom.equals("kode_brg")) {
                insertQuery = "UPDATE barang SET kode_barang = ? WHERE " + where + " = ?";
                ps = conn.prepareStatement(insertQuery);
                ps.setString(1, key);
                ps.setString(2, whereKey);
            } else if (setKolom.equals("nama_barang")) {
                insertQuery = "UPDATE barang SET nama_barang = ? WHERE " + where + " = ?";
                ps = conn.prepareStatement(insertQuery);
                ps.setString(1, key);
                ps.setString(2, whereKey);
            } else if (setKolom.equals("satuan")) {
                insertQuery = "UPDATE barang SET satuan = ? WHERE " + where + " = ?";
                ps = conn.prepareStatement(insertQuery);
                ps.setString(1, key);
                ps.setString(2, whereKey);
            } else if (setKolom.equals("stok")) {
                insertQuery = "UPDATE barang SET stok = ? WHERE " + where + " = ?";
                ps = conn.prepareStatement(insertQuery);
                ps.setInt(1, Integer.parseInt(key));
                ps.setString(2, whereKey);
            } else if (setKolom.equals("harga_beli")) {
                insertQuery = "UPDATE barang SET harga_beli = ? WHERE " + where + " = ?";
                ps = conn.prepareStatement(insertQuery);
                ps.setInt(1, Integer.parseInt(key));
                ps.setString(2, whereKey);
            } else if (setKolom.equals("harga_jual")) {
                insertQuery = "UPDATE barang SET harga_jual = ? WHERE " + where + " = ?";
                ps = conn.prepareStatement(insertQuery);
                ps.setInt(1, Integer.parseInt(key));
                ps.setString(2, whereKey);
            } else if (setKolom.equals("stok_minimal")) {
                insertQuery = "UPDATE barang SET stok_minimal = ? WHERE " + where + " = ?";
                ps = conn.prepareStatement(insertQuery);
                ps.setInt(1, Integer.parseInt(key));
                ps.setString(2, whereKey);
            } else {

                JOptionPane.showMessageDialog(null, "Field Tidak Ditemukan");
                return;
            }

            ps.executeUpdate();

            ps.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
