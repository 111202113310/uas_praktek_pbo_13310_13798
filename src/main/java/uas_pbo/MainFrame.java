package uas_pbo;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import uas_pbo.models.connection;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

public class MainFrame extends JFrame {
    final Font mainFont = new Font("Times New Roman", Font.BOLD, 18);
    static JTextField FieldKodeBrg, FieldNamaBrg, FieldSatuanBrg,
            FieldStokBrg, FieldMinBrg, FieldHargaBeli, FieldHargaJual, FieldStok, usernameTextField;
    private JPasswordField passwordField;
    private JTable table;
    public int active = 0;
    Boolean isLoggedIn = false;

    static Connection conn;
    static Statement stmt;
    static ResultSet rs;

    public void init() {
        connection connection = new connection();
        conn = connection.getCon();

        JLabel usernameLabel = new JLabel("Username:");
        usernameTextField = new JTextField(20);

        JLabel passwordLabel = new JLabel("Password:");
        passwordField = new JPasswordField(20);

        JButton loginButton = new JButton("Login");

        // ================================================================================================================================
        // ================================================================================================================================
        // ================================================================================================================================
        // ================================================================================================================================
        // ================================================================================================================================
        // ================================================================================================================================
        // ================================================================================================================================
        // ================================================================================================================================

        JLabel labelKodeBrg = new JLabel("Kode Barang");
        labelKodeBrg.setFont(mainFont);
        FieldKodeBrg = new JTextField();
        FieldKodeBrg.setFont(mainFont);

        JLabel labelNamaBrg = new JLabel("Nama Barang");
        labelNamaBrg.setFont(mainFont);
        FieldNamaBrg = new JTextField();
        FieldNamaBrg.setFont(mainFont);

        JLabel labelSatuan = new JLabel("Satuan Barang");
        labelSatuan.setFont(mainFont);
        FieldSatuanBrg = new JTextField();
        FieldSatuanBrg.setFont(mainFont);

        JLabel labelStokBrg = new JLabel("Stok Barang");
        labelStokBrg.setFont(mainFont);
        FieldStokBrg = new JTextField();
        FieldStokBrg.setFont(mainFont);

        JLabel LabelHargaBeli = new JLabel("Harga Beli");
        LabelHargaBeli.setFont(mainFont);
        FieldHargaBeli = new JTextField();
        FieldHargaBeli.setFont(mainFont);

        JLabel labelHargaJual = new JLabel("Harga Jual");
        labelHargaJual.setFont(mainFont);
        FieldHargaJual = new JTextField();
        FieldHargaJual.setFont(mainFont);

        JLabel labelStok = new JLabel("Stok");
        labelStok.setFont(mainFont);
        FieldStok = new JTextField();
        FieldStok.setFont(mainFont);

        JLabel labelMinBrg = new JLabel("Stok Minimal");
        labelMinBrg.setFont(mainFont);
        FieldMinBrg = new JTextField();
        FieldMinBrg.setFont(mainFont);

        // Button
        JButton btnInsert = new JButton("Tambah");
        btnInsert.setFont(mainFont);
        btnInsert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String kodeBrg = FieldKodeBrg.getText();
                String namaBrg = FieldNamaBrg.getText();
                String satuanBrg = FieldSatuanBrg.getText();
                int hargabeli = Integer.parseInt(FieldHargaBeli.getText());
                int hargajual = Integer.parseInt(FieldHargaJual.getText());
                int stok = Integer.parseInt(FieldStokBrg.getText());
                int stokMin = Integer.parseInt(FieldMinBrg.getText());
                insert(kodeBrg, namaBrg, satuanBrg, hargabeli, hargajual, stok, stokMin);

            }
        });

        JButton btnDelete = new JButton("Hapus");
        btnDelete.setFont(mainFont);
        btnDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                FramePageDelete pd = new FramePageDelete();
                pd.Delete();
            }
        });

        JButton btnUpdate = new JButton("Perbarui");
        btnUpdate.setFont(mainFont);
        btnUpdate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                FramePageUpdate pu = new FramePageUpdate();
                pu.Update();

            }
        });

        JButton btnExport = new JButton("Export PDF");
        btnExport.setFont(mainFont);
        btnExport.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                exportPDF ePdf = new exportPDF();
                ePdf.export();
            }
        });

        JButton btnRefresh = new JButton("Refresh");
        btnRefresh.setFont(mainFont);
        btnRefresh.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                refreshTable();
            }
        });

        JButton btnclr = new JButton("Hapus Semua");
        btnclr.setFont(mainFont);
        btnclr.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FieldKodeBrg.setText("");
                FieldNamaBrg.setText("");
                FieldSatuanBrg.setText("");
                FieldHargaBeli.setText("");
                FieldHargaJual.setText("");
                FieldStokBrg.setText("");
                FieldMinBrg.setText("");
            }
        });

        DefaultTableModel tableModel = getTable.getTableModel();
        table = new JTable(tableModel);
        JScrollPane scrollPane = new JScrollPane(table);

        JPanel buttJPanel = new JPanel();
        buttJPanel.setPreferredSize(new Dimension(150, 50));
        buttJPanel.setLayout(new GridLayout(1, 6, 5, 5));
        buttJPanel.add(btnInsert);
        buttJPanel.add(btnDelete);
        buttJPanel.add(btnUpdate);
        buttJPanel.add(btnExport);
        buttJPanel.add(btnRefresh);
        buttJPanel.add(btnclr);

        JPanel formPanel = new JPanel();
        formPanel.setSize(400, 700);
        formPanel.setLayout(new GridLayout(7, 2, 5, 5));
        formPanel.add(labelKodeBrg);
        formPanel.add(labelNamaBrg);
        formPanel.add(labelSatuan);
        formPanel.add(LabelHargaBeli);
        formPanel.add(labelHargaJual);
        formPanel.add(labelStokBrg);
        formPanel.add(labelMinBrg);

        JPanel formPanelCenter = new JPanel();
        formPanelCenter.setLayout(new BorderLayout());
        formPanelCenter.setBorder(new EmptyBorder(0, 100, 0, 50));
        formPanelCenter.setLayout(new GridLayout(7, 2, 5, 5));
        formPanelCenter.add(FieldKodeBrg);
        formPanelCenter.add(FieldNamaBrg);
        formPanelCenter.add(FieldSatuanBrg);
        formPanelCenter.add(FieldHargaBeli);
        formPanelCenter.add(FieldHargaJual);
        formPanelCenter.add(FieldStokBrg);
        formPanelCenter.add(FieldMinBrg);

        JPanel loginPanel = new JPanel();
        loginPanel.setSize(400, 700);
        loginPanel.setLayout(new GridLayout(3, 2, 5, 5));
        loginPanel.add(usernameLabel);
        loginPanel.add(usernameTextField);
        loginPanel.add(passwordLabel);
        loginPanel.add(passwordField);

        JPanel mainPanelLogin = new JPanel();
        mainPanelLogin.setLayout(new BorderLayout());
        mainPanelLogin.add(loginPanel, BorderLayout.CENTER);
        mainPanelLogin.add(loginButton, BorderLayout.SOUTH);

        JPanel mainpanel = new JPanel();
        mainpanel.setLayout(new BorderLayout());
        mainpanel.add(formPanel, BorderLayout.WEST);
        mainpanel.add(formPanelCenter, BorderLayout.CENTER);
        mainpanel.add(scrollPane, BorderLayout.EAST);
        mainpanel.add(buttJPanel, BorderLayout.SOUTH);

        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String inputUsername = usernameTextField.getText();
                String inputPassword = new String(passwordField.getPassword());

                try {
                    connection.KoneksiMySQL();
                    Connection conn = connection.getCon();
                    Statement statement = connection.getStm();

                    String query = "SELECT active FROM user WHERE username = '" + inputUsername + "' AND password = '"
                            + inputPassword + "'";
                    ResultSet resultSet = statement.executeQuery(query);

                    if (resultSet.next()) {
                        active = resultSet.getInt("active");
                        if (active == 1) {
                            isLoggedIn = true;
                        } else {
                            JOptionPane.showMessageDialog(null, "admin sudah tidak aktif", "Error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Username dan password tidak valid", "Error",
                                JOptionPane.ERROR_MESSAGE);
                    }

                    statement.close();
                    conn.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

                if (isLoggedIn) {
                    add(mainpanel);
                    mainpanel.setVisible(true);
                    mainPanelLogin.setVisible(false);
                    refreshTable();
                } else {
                    mainpanel.setVisible(false);
                    mainPanelLogin.setVisible(true);
                }

            }
        });

        add(mainPanelLogin);

        setTitle("CRUD");
        setSize(900, 400);
        setLocationByPlatform(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public void insert(String kodeBrg, String namaBrg, String satuanBrg, int hargaBeli, int hargaJual, int stok,
            int stokMin) {
        try {
            connection connection = new connection();
            connection.KoneksiMySQL();
            conn = connection.getCon();
            stmt = connection.getStm();

            String checkQuery = "SELECT COUNT(*) AS count FROM barang WHERE kode_barang = ?";
            PreparedStatement checkPs = conn.prepareStatement(checkQuery);
            checkPs.setString(1, kodeBrg);
            ResultSet checkResult = checkPs.executeQuery();
            if (checkResult.next()) {
                int count = checkResult.getInt("count");
                if (count > 0) {
                    JOptionPane.showMessageDialog(null, "Database sudah ada");
                    System.out.println("Kode barang sudah ada dalam database");
                    checkResult.close();
                    checkPs.close();
                    conn.close();
                    return;
                }
            }
            checkResult.close();
            checkPs.close();

            stmt = conn.createStatement();

            String insertQuery = "INSERT INTO barang (kode_barang, nama_barang, satuan, harga_beli, harga_jual , stok, stok_minimal) VALUES (?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement ps = conn.prepareStatement(insertQuery);
            ps.setString(1, kodeBrg);
            ps.setString(2, namaBrg);
            ps.setString(3, satuanBrg);
            ps.setInt(4, hargaBeli);
            ps.setInt(5, hargaJual);
            ps.setInt(6, stok);
            ps.setInt(7, stokMin);

            ps.execute();

            ps.close();
            stmt.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void refreshTable() {
        DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
        tableModel.setRowCount(0); // Hapus semua baris tabel

        try {
            connection connection = new connection();
            connection.KoneksiMySQL();
            conn = connection.getCon();
            stmt = connection.getStm();

            String query = "SELECT * FROM barang";
            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                // Ambil data dari ResultSet
                String kodeBrg = rs.getString("kode_barang");
                String namaBrg = rs.getString("nama_barang");
                String satuanBrg = rs.getString("satuan");
                int hargaBeli = rs.getInt("harga_beli");
                int hargaJual = rs.getInt("harga_jual");
                int stok = rs.getInt("stok");
                int stokMin = rs.getInt("stok_minimal");

                // Tambahkan data ke model tabel
                Object[] row = { kodeBrg, namaBrg, satuanBrg, hargaBeli, hargaJual, stok, stokMin };
                tableModel.addRow(row);
            }

            rs.close();
            stmt.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
