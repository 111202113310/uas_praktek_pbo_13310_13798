package uas_pbo;

import java.sql.Connection;
import java.util.HashMap;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import uas_pbo.models.connection;

public class exportPDF {
    private Connection conn;

    public void export() {
        try {
            connection connection = new connection();
            connection.KoneksiMySQL();
            conn = connection.getCon();
            JasperReport jasperReport = JasperCompileManager.compileReport(
                    "D:\\uas-pbo\\Invoice.jrxml");

            // Mengisi laporan dengan data dari database
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap<>(), conn);

            // Export laporan ke file PDF
            String outputFilePath = "D:\\uas-pbo\\laporan\\uas_pbo.pdf";
            JasperExportManager.exportReportToPdfFile(jasperPrint, outputFilePath);

            System.out.println("Laporan PDF berhasil diekspor ke " + outputFilePath);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
