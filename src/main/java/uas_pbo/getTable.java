package uas_pbo;

import javax.swing.table.DefaultTableModel;

import uas_pbo.models.connection;

import java.sql.*;

public class getTable {
    public static DefaultTableModel getTableModel() {
        DefaultTableModel tableModel = new DefaultTableModel();

        try {
            connection connection = new connection();
            connection.KoneksiMySQL();

            // Membuat statement SQL
            Statement statement = connection.getCon().createStatement();
            String query = "SELECT kode_barang, nama_barang, satuan, harga_beli, harga_jual, stok, stok_minimal FROM barang";
            ResultSet resultSet = statement.executeQuery(query);

            // Mendapatkan metadata kolom
            ResultSetMetaData metaData = resultSet.getMetaData();
            int columnCount = metaData.getColumnCount();

            // Menambahkan kolom ke model tabel
            for (int col = 1; col <= columnCount; col++) {
                tableModel.addColumn(metaData.getColumnName(col));
            }

            // Menambahkan data ke model tabel
            while (resultSet.next()) {
                Object[] rowData = new Object[columnCount];
                for (int col = 1; col <= columnCount; col++) {
                    rowData[col - 1] = resultSet.getObject(col);
                }
                tableModel.addRow(rowData);
            }

            // Menutup statement dan koneksi
            statement.close();
            connection.getCon().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return tableModel;
    }
}
